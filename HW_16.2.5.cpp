﻿#include <iostream>
#include <ctime>

int main()
{
    const int N = 10;
    int SumEl = 0;

    //Индекс строки
    time_t t = time(0);
    tm Localtime;
    int IndexSum = 0;

    setlocale(LC_CTYPE, "rus");
    localtime_s(&Localtime, &t);
    IndexSum = (&Localtime)->tm_mday % N;

    //Инициализация и вывод массива
    int array[N][N];

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
            if (i == IndexSum) SumEl += array[i][j]; // сложение элементов нужной строки
        }
        std::cout << "\n";
    }

    std::cout << "\n" << "Sum of row " << IndexSum << ": " << SumEl << "\n";
}